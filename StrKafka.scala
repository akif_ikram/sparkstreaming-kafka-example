package streamingKafkaExample

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql._
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.SQLContext._
import org.apache.spark.sql.functions._
import org.elasticsearch.spark._
import org.elasticsearch.spark.sql._

import org.apache.log4j.Logger
import org.apache.log4j.Level


    case class nameInput (
      nameId: Int,
      nameEventTime: java.sql.Timestamp,
      fName: String,
      lName: String
    )

    case class addressInput (
      addressId: Int,
      addressEventTime: java.sql.Timestamp,
      address: String,
      phone: String
    )

    case class Person (
          fName: String,
          lName: String,
          address: String,
          phone: String
       )

object StreamKafka {
  def main (args: Array[String]) {
    Logger.getLogger("org").setLevel(Level.WARN)

    val sc = new SparkContext(new SparkConf())

    val spark=SparkSession.builder().appName("StreamKafka").getOrCreate()
    import spark.implicits._

    val namesK = spark
          .readStream
          .format("kafka")
          .option("kafka.bootstrap.servers", "localhost:9092")
          .option("subscribe", "one")
          .load()
          .selectExpr("CAST(value AS STRING)")
          .as[String]

    val names = namesK
          .map(_.split(','))
          .select ( $"value"(0).cast("Int") as "nameId",
                    to_timestamp($"value"(1)) as "nameEventTime",
                    $"value"(2) as "fName",
                    $"value"(3) as "lName"
                  )
          .as[nameInput]
          .withWatermark("nameEventTime", "1 minutes")

    val addressK = spark
          .readStream
          .format("kafka")
          .option("kafka.bootstrap.servers", "localhost:9092")
          .option("subscribe", "two")
          .load()
          .selectExpr("CAST(value AS STRING)")
          .as[String]

    val address = addressK
          .map(_.split(','))
          .select ( $"value"(0).cast("Int") as "addressId",
                    to_timestamp($"value"(1)) as "addressEventTime",
                    $"value"(2) as "address",
                    $"value"(3) as "phone"
                  )
          .as[addressInput]
          .withWatermark("addressEventTime", "5 minutes")


    val joinedDs1 = names.join (address,
                       expr(""" nameId = addressId AND
                                addressEventTime >= nameEventTime AND
                                addressEventTime <= nameEventTime + interval 20 seconds
                             """)
                     )
    val joinedDs2 = joinedDs1.select ($"nameEventTime", $"addressEventTime", $"fName", $"lName", $"address", $"phone").as[Person]
    val joinedCounts = joinedDs2.groupBy($"addressEventTime").count()

    val query = joinedDs2.writeStream
      .outputMode("append")
      .format("console")
      .start()

    query.awaitTermination()

    println ("done")
  }
}
