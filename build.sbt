ThisBuild / scalaVersion := "2.11.8"

val sparkVersion = "2.3.1"

val sparkCore = "org.apache.spark" %% "spark-core" % sparkVersion
val sparkSql = "org.apache.spark" %% "spark-sql" % sparkVersion
val elasticSpark = "org.elasticsearch" %% "elasticsearch-spark-20" % "6.3.0"

resolvers ++= Seq(
  "apache-snapshots" at "http://repository.apache.org/snapshots/"
)

lazy val StrKafka = (project in file("."))
  .settings(
    name := "StrKafka",
    libraryDependencies ++= Seq(
      sparkCore,
      sparkSql,
      elasticSpark
    )
  )
